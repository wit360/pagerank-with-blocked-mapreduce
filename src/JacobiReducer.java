import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

/**
 * locally calculate PageRank of pages within the block until converged
 * @author wt255
 */
public class JacobiReducer extends MapReduceBase implements Reducer<LongWritable, Edge, LongWritable, Text> {
	// for logging
	private Log log = LogFactory.getLog(JacobiReducer.class);
	
	// constants
	private int    N; 			// page count
	private double d; 			// damp factor
	private double epsilon; 	// error threshold
	private double threshold;	// epsilon*numberOfPages in this block
	private long   dummyPage;	// used to represent pages without outgoing link
	
	private long   firstPage;	// lowest page number assigned to this block
	private long   lastPage; 	// highest page number assigned to this block
	
	// read constants from job
	public @Override void configure(JobConf job){
		N 	      = Integer.parseInt(job.get("pagecount"));
		d         = Double.parseDouble(job.get("dampfactor"));
		epsilon   = Double.parseDouble(job.get("epsilon"));
		dummyPage = Integer.parseInt(job.get("dummypage"));
		if(N==6) Edge.debug = true;
	}
	
	// basic data structure for our subgraph
	// all Edge into this block (key=srcID, values=list of edges from srcID)
	HashMap<Long,ArrayList<Edge>> alledgesSrc = new HashMap<Long,ArrayList<Edge>>();	
	HashMap<Long,ArrayList<Edge>> alledgesDst = new HashMap<Long,ArrayList<Edge>>();	
	
	// PageRank vector for iteration
	int iterations = 0;
	HashMap<Long,Double>  PR       = new HashMap<Long,Double>();  // PageRank of internal pages
	HashMap<Long,Double>  PR_prev  = new HashMap<Long,Double>();
	
	public @Override void reduce(LongWritable blockNum, Iterator<Edge> edges,
			OutputCollector<LongWritable, Text> output, Reporter reporter) throws IOException {
		
		log.info(String.format("reducer() blockNum=%d",blockNum.get()));
		
		// clear everything...
		iterations = 0;
		alledgesSrc.clear();
		alledgesDst.clear();
		PR.clear();
		PR_prev.clear();
		
		// get page range in this block
		firstPage = Edge.getBlockFirstPage(blockNum.get());
		lastPage = Edge.getBlockLastPage(blockNum.get());
		
		// collect all received edges and put into 2-way map for fast lookup
		// calculate degree of internal source node, and save PR of external source node
		// - for each <u,v> deg[u]++      ,for u in this block
		//                  set PR_ext[u] ,for u from external blocks
		while(edges.hasNext()){
			Edge tmp_edge = edges.next();
			Edge edge = new Edge(tmp_edge);
			
			// log.info(String.format(" - edge: %s",edge.toString()));
			if(!alledgesSrc.containsKey(edge.srcID)) alledgesSrc.put(edge.srcID,new ArrayList<Edge>());
			alledgesSrc.get(edge.srcID).add(edge);
			if(!alledgesDst.containsKey(edge.dstID)) alledgesDst.put(edge.dstID,new ArrayList<Edge>());
			alledgesDst.get(edge.dstID).add(edge);
		}
		
		// initialize PR[v] for all page v in this block
		for(long v=firstPage; v<=lastPage; v++){
			if( !alledgesSrc.containsKey(v) ) continue; // if page v does not exists!
			PR.put(v, alledgesSrc.containsKey(v) ? alledgesSrc.get(v).get(0).srcPR : (1.0)/N);
			PR_prev.put(v, 999.0);
			// log.info("init PR[" + v + "]=" + PR.get(v) + " " + (1.0/N));
		}
		
		threshold = epsilon*PR.size(); // calculate stop threshold
		this.printDataStructure();
		
		// main iteration loop
		dumpPR("before main loop");
		while(!isConverged() && iterations < 100) {
			iterations++;
			// PR_prev := PR
			for(long v=firstPage; v<=lastPage; v++) {
				if( !alledgesSrc.containsKey(v) ) continue; // if page v does not exists!
				PR_prev.put(v, PR.get(v));
				PR.put(v,0.0);
			}
			
			// iteration: for all v in the block and for each edge <u,v>
			// - for each source page u in the block PR[v] += PR_prev[u]/deg[u]
			//                          otherwise    PR[v] += R[u]
			// - normalize with damping factor       PR[v] = d*PR[v] + (1-d)
			for(long v=firstPage; v<=lastPage; v++){     // for all v in the block
				if( !alledgesSrc.containsKey(v) ) continue; // if page v does not exists!
				// log.info("SOP for " + v);
				if(alledgesDst.containsKey(v)) {
					for(Edge edge : alledgesDst.get(v)){ // for each edge <u,v>
						if( edge.isSrcInBlock(blockNum.get()) ) { 
							// for internal source node PR[v] += PR_prev[u]/deg[u]
							// log.info("- INT " + edge.toString() + " PR[u]/deg[u]=" + PR_prev.get(u)/edge.srcDeg);
							increasePR( v, PR_prev.get(edge.srcID)/edge.srcDeg );
						} else {
							// for external source node PR[v] += recvPR[u]/deg[u]
							// log.info("- EXT " + edge.toString() + " PR[u]=" + edge.srcPR/edge.srcDeg);
							increasePR( v, edge.srcPR/edge.srcDeg );
						}
					}
				} 
				// normalize with damping factor
				PR.put(v, d*PR.get(v) + (1-d)/N);
			}
			dumpPR("after iteration: " + iterations);
		}
		
		// generate output
		for(long v=firstPage; v<=lastPage; v++){
			if( !alledgesSrc.containsKey(v) ) continue; // if page v does not exists!
			double origPR  = alledgesSrc.get(v).get(0).srcPR;
			double deltaPR = Math.abs(PR.get(v)-origPR)/PR.get(v);
			if( alledgesSrc.get(v).get(0).srcDeg != 0) {
				long deg = alledgesSrc.get(v).get(0).srcDeg;
				ArrayList<Long> dsts = new ArrayList<Long>();
				for(Edge edge : alledgesSrc.get(v)) dsts.add(edge.dstID);
				
				StringBuffer dstList = new StringBuffer();
				for(Long l:dsts){
					dstList.append(String.format(" %6d",l));
				}
				// key=srcID  value=PR  error  deg  [dst-list]
				output.collect(new LongWritable(v), new Text(String.format("%.11f %.11f %5d   %s",
						PR.get(v),deltaPR,deg,dstList.toString())) );
			} else {
				// key=srcID  value=PR  error  0    [dummypage]
				output.collect(new LongWritable(v), new Text(String.format("%.11f %.11f %5d    %d",
						PR.get(v),deltaPR,0,dummyPage)) ); // no outlink...
			}
		}
		
		// collect output from all pages in this block
		reporter.getCounter(JacobiCounter.TOTAL_BLOCK_ITERATIONS).increment(iterations);
		reporter.getCounter(JacobiCounter.TOTAL_ERROR_10_EXP_8).increment((long)(100000000L*getError()));
	}
	
	/**
	 * @return true if sum of error between PR and PR_prev is less than threshold (eN) where e=0.001
	 */
	private boolean isConverged(){
		double error = 0.0;
		for(long page : PR.keySet()){
			if(!alledgesSrc.containsKey(page)) continue;
			error += Math.abs(PR.get(page)-PR_prev.get(page))/PR.get(page);
			if( error > threshold ) {
				log.info(String.format(" - isConverged()=false, errorSum=%f, threshold=%f",error,threshold));
				return false;
			}
		}
		log.info(String.format(" - isConverged()=true, errorSum=%f, threshold=%f",error,threshold));
		return true;
	}
	
	/**
	 * @return sum of total delta between PR at the beginning and PR at the end   !!! it is DIFFERENT from isConverged() !!!
	 *         this value will be reported thru Hadoop Counter to determine whether another iteration is required
	 */
	private double getError(){
		double error = 0.0;
		for(long page : PR.keySet()){
			double origPR = alledgesSrc.get(page).get(0).srcPR;
			error += Math.abs( (PR.get(page)-origPR)/PR.get(page));
		}
		return error;
	}
	
	// utilitiy functions
	/**
	 * PR[id] += value
	 * @param id
	 * @param value
	 */
	private void increasePR(long id, double value){
		if(PR.containsKey(id)) PR.put(id, PR.get(id)+value);
		else PR.put(id, value);
	}
	
	/**
	 * print out all local data structure
	 */
	private void printDataStructure(){
		/*
		log.info("printDataStructure()");
		for(Long srcPage:alledgesSrc.keySet()) {
			for(Edge e: alledgesSrc.get(srcPage)){
				log.info(" - " + e.toString() + " PR[dst]=" + getMap(PR, e.dstID));
			}
		}
		*/
	}
	
	/**
	 * @param map
	 * @param key
	 * @return map[key] if key exists otherwise 0.0
	 */
	public static double getMap(Map<Long, Double> map, long key){
		if(map.containsKey(key)) return map.get(key);
		return 0.0;
	}
	
	/**
	 * print out PR[v] for all v in this block
	 * @param location
	 */
	private void dumpPR(String location){
		/*
		log.info("dumpPR() " + location);
		for(long v=firstPage; v<=lastPage; v++){
			log.info(String.format(" - PR[%d] = %f",v,PR.get(v)));
		}
		*/
	}
}
