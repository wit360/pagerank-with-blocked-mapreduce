import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

/**
 * emit each edge to source node block and destination node block
 * for each page without outgoing link, emit an edge with dummy destination
 * @author wt255
 */
public class JacobiMapper extends MapReduceBase implements Mapper<LongWritable,Text,LongWritable,Edge>{
	// for logging
	Log log = LogFactory.getLog(JacobiMapper.class);
	
	// constants
	private int  N;				// number of page
	private long dummyPage;		// for page without outgoing link
	public @Override void configure(JobConf job){
		N = Integer.parseInt(job.get("pagecount"));
		dummyPage = Long.parseLong(job.get("dummypage"));
		if(N==6) Edge.debug = true;
	}
	
	public @Override void map(LongWritable key, Text value, OutputCollector<LongWritable,Edge> output,
							  Reporter reporter) throws IOException {
		log.info(String.format("map(%d,%s)",key.get(),value.toString()));
		String[] tokens = value.toString().trim().split("\\s+");
		// 0          1          2        3           4 .............. N
		// srcID      PR[srcID]  error    out-degree  [destination list]
		
		// fill the prototype Edge with source page information
		Edge src = new Edge();
		src.srcID  = Long.parseLong(tokens[0]);
		src.srcPR  = Double.parseDouble(tokens[1]);
		src.srcDeg = Long.parseLong(tokens[3]);
		
		for(int i=4; i<tokens.length; i++){
			Edge edge  = new Edge(src);  // create a new edge to emit to reducer based on the prototype
			edge.dstID = Long.parseLong(tokens[i]);
			
			// emit the edge to the source block
			output.collect(new LongWritable(edge.getSrcBlockNum()), edge);
			
			// emit the edge to the destination block if it is different from the source
			if(!edge.isEdgeLocal() && edge.dstID!=dummyPage) {
				output.collect(new LongWritable(edge.getDstBlockNum()), edge);
			}
		}
	}
}