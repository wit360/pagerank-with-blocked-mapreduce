import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.mapreduce.Job;

/**
 * This MapReduce job transform the original input of the form:
 *   srcID  dstID  randomNum
 *   
 * into the form suitable for PageRank iteration:
 *   srcID  PR  error  out-degree   [destination-lists]
 *   
 * During transformation it also ignore edges with randomNum in the reject range specified in the writeup.
 * Hadoop Counter, JacobiCounter.TOTAL_REJECTED, is used to keep track of the number of rejected edges.
 * 
 * @author wt255
 */
public class ParseInput {
	
	// Convert each line to edge then output:
	// - emit(srcID, edge)
	// - emit(dstID, edge)
	public static class ParseMapper extends MapReduceBase implements Mapper<LongWritable,Text,LongWritable,Edge>{
		public @Override void map(LongWritable key, Text value, OutputCollector<LongWritable, Edge> output, Reporter reporter) throws IOException {
			// parse input tokens from the original file
			// 0          1          2            
			// srcID      dstID      probability 
			String[] tokens = value.toString().trim().split("\\s+");
			
			// test if we have to reject this edge
			double probability = Double.parseDouble(tokens[2]);
			if(!Edge.selectInputLine(probability)) {
				reporter.incrCounter(JacobiCounter.TOTAL_REJECTED, 1L);
				return;
			}
			
			// emit each edge for both source page and destination page
			Edge edge = new Edge();
			edge.srcID = Integer.parseInt(tokens[0]);
			edge.dstID = Integer.parseInt(tokens[1]);
			
			output.collect(new LongWritable(edge.srcID), edge);
			output.collect(new LongWritable(edge.dstID), edge);
		}
	}
	
	// take input <pageId, [edge1, edge2, ..., edgeN]>
	// and emit <pageId, "PR error out-degree [destination-list]">"
	// where
	//    PR         = default PageRank = 1/N
	//    error      = default error    = 1.0 (not used by the program)
	//    out-degree = number of pages that this page links to (calculate by examining all received edges)
	//    [destination-list] = list of dstID that this page links to
	public static class ParseReducer extends MapReduceBase implements Reducer<LongWritable,Edge,LongWritable,Text> {
		private double defaultPR;
		private long   dummyPage;
		public @Override void configure(JobConf job){
			defaultPR = 1.0/Integer.parseInt(job.get("pagecount"));
			dummyPage = Integer.parseInt(job.get("dummypage"));
		}
		public @Override void reduce(LongWritable source, Iterator<Edge> edges, OutputCollector<LongWritable, Text> output, Reporter reporter) throws IOException {
			ArrayList<Long> dsts = new ArrayList<Long>();       // collect edges from the source page
			while(edges.hasNext()){
				Edge edge = edges.next();
				if(source.get() != edge.srcID) continue;
				dsts.add(edge.dstID);
			}
			StringBuffer dstList = new StringBuffer();
			for(Long l:dsts){
				dstList.append(String.format(" %6d",l));        // produce [destinaion-list] string
			}
			if(dsts.isEmpty()) dstList.append(" " + dummyPage); // for page without outgoing link
			
			// output <key=srcID, val="PR error deg [destination-list]">
			output.collect(source, new Text(String.format("%.11f %.11f %5d   %s",defaultPR,1.0,dsts.size(),dstList)));
		}
	}
	
	/**
	 * 
	 * @param inputPath
	 * @return new inputPath for parsed file
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public static String runParseJob(String inputPath) throws IOException, InterruptedException, ClassNotFoundException{
		JobConf conf = new JobConf(ParseInput.class);
		
		// constants
		conf.set("pagecount", String.valueOf(JacobiJob.pageCount));  	   
		conf.set("dummypage", String.valueOf(JacobiJob.dummyPage));
		String outputPath = inputPath + "parsed";
		
        // configure mapper and reducer
        conf.setMapperClass(ParseMapper.class);
        conf.setReducerClass(ParseReducer.class);
        
        // configure key/value data types
        conf.setMapOutputKeyClass(LongWritable.class);
        conf.setMapOutputValueClass(Edge.class);
        conf.setOutputKeyClass(Edge.class);
        conf.setOutputValueClass(Text.class);
        
        // hdfs file i/o
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
        
        TextInputFormat.addInputPath(conf, new Path(inputPath));
	     // configuration should contain reference to your namenode
        FileSystem fs = FileSystem.get(new Configuration());
        // true stands for recursively deleting the folder you gave
        try{ fs.delete(new Path(outputPath), true);
        } catch(Exception e){}
        TextOutputFormat.setOutputPath(conf, new Path(outputPath));
        
        Job parseJob = new Job(conf);
        parseJob.waitForCompletion(true);
        
        return outputPath;
	}
}
