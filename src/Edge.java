import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * represent an edge
 * it also provides utility functions for edge processing
 * 
 * @author wt255
 */
class Edge implements Writable {
	public long   srcID;					// source page
	public long	  dstID;					// destination page
	public double srcPR;					// PageRank of the source page
	public long   srcDeg;					// out-degree of source page
	public static boolean debug = false;
	
	public Edge(){}
	/**
	 * create a new edge object copying contents from e
	 * @param e
	 */
	public Edge(Edge e){
		srcID  = e.srcID;
		dstID  = e.dstID;
		srcDeg = e.srcDeg;
		srcPR  = e.srcPR;
	}
	
	/**
	 * @param B - block number
	 * @return true if the srcID is in the given block B
	 */
	public boolean isSrcInBlock(long B){
		return blockNum(srcID) == B;
	}
	
	/**
	 * @param B - block number
	 * @return true if the dstID is in the given block B
	 */
	public boolean isDstInBlock(long B){
		return blockNum(dstID) == B;
	}
	
	/**
	 * @return blockNum of source page
	 */
	public long getSrcBlockNum(){
		return blockNum(srcID);
	}
	
	/**
	 * @return blockNum of destiination page
	 */
	public long getDstBlockNum(){
		return blockNum(dstID);
	}
	
	/**
	 * @return true if the edge does not span multiple blocks
	 */
	public boolean isEdgeLocal(){
		return blockNum(srcID)==blockNum(dstID);
	}
	
	// required for Hadoop value type deserialization
	public @Override void readFields(DataInput in) throws IOException {
		srcID  = in.readLong();
		srcPR  = in.readDouble();
		srcDeg = in.readLong();
		dstID  = in.readLong();
	}

	// required for Hadoop value type serialization
	public @Override void write(DataOutput out) throws IOException {
		out.writeLong(srcID);
		out.writeDouble(srcPR);
		out.writeLong(srcDeg);
		out.writeLong(dstID);
	}
	
	public @Override boolean equals(Object other){ 
		return srcID == ((Edge)other).srcID && dstID == ((Edge)other).dstID;
	}
	
	public @Override int hashCode(){
		return 13*(int)srcID + 175911*(int)dstID;
	}
	
	// below are some static utility methods
	// determine if the edge should be included
	private static double fromNetID   = 0.552; // from wt255
	private static double rejectMin   = 0.99*fromNetID;
	private static double rejectLimit = rejectMin + 0.01;
	
	/**
	 * @param x
	 * @return true if x falls outside the reject range. false otherwise
	 */
	public static boolean selectInputLine(double x){
		return !(x>=rejectMin && x<rejectLimit); // [0.54648, 0.55648]
	}
	
	/**
	 * @param id
	 * @return block number of the given page id
	 */
	public static long blockNum(long id){
		// for DEBUG with small data
		if(debug){ 
			if(id < 3) return 0;
			if(id < 6) return 1;
		}
		
		if(id <  10328) return 0;
		if(id <  20373) return 1;
		if(id <  30629) return 2;
		if(id <  40645) return 3;
		if(id <  50462) return 4;
		if(id <  60841) return 5;
		if(id <  70591) return 6;
		if(id <  80118) return 7;
		if(id <  90497) return 8;
		if(id < 100501) return 9;
		if(id < 110567) return 10;
		if(id < 120945) return 11;
		if(id < 130999) return 12;
		if(id < 140574) return 13;
		if(id < 150953) return 14;
		if(id < 161332) return 15;
		if(id < 171154) return 16;
		if(id < 181514) return 17;
		if(id < 191625) return 18;
		if(id < 202004) return 19;
		if(id < 212383) return 20;
		if(id < 222762) return 21;
		if(id < 232593) return 22;
		if(id < 242878) return 23;
		if(id < 252938) return 24;
		if(id < 263149) return 25;
		if(id < 273210) return 26;
		if(id < 283473) return 27;
		if(id < 293255) return 28;
		if(id < 303043) return 29;
		if(id < 313370) return 30;
		if(id < 323522) return 31;
		if(id < 333883) return 32;
		if(id < 343663) return 33;
		if(id < 353645) return 34;
		if(id < 363929) return 35;
		if(id < 374236) return 36;
		if(id < 384554) return 37;
		if(id < 394929) return 38;
		if(id < 404712) return 39;
		if(id < 414617) return 40;
		if(id < 424747) return 41;
		if(id < 434707) return 42;
		if(id < 444489) return 43;
		if(id < 454285) return 44;
		if(id < 464398) return 45;
		if(id < 474196) return 46;
		if(id < 484050) return 47;
		if(id < 493968) return 48;
		if(id < 503752) return 49;
		if(id < 514131) return 50;
		if(id < 524510) return 51;
		if(id < 534709) return 52;
		if(id < 545088) return 53;
		if(id < 555467) return 54;
		if(id < 565846) return 55;
		if(id < 576225) return 56;
		if(id < 586604) return 57;
		if(id < 596585) return 58;
		if(id < 606367) return 59;
		if(id < 616148) return 60;
		if(id < 626448) return 61;
		if(id < 636240) return 62;
		if(id < 646022) return 63;
		if(id < 655804) return 64;
		if(id < 665666) return 65;
		if(id < 675448) return 66;
		if(id < 685230) return 67;
		return 68;
	}
	
	/**
	 * @param blockNum
	 * @return the first page number assigned to blockNum
	 */
	public static long getBlockFirstPage(long blockNum){
		// for testing with 6 nodes (2 blocks)
		if(debug) {
			if(blockNum==0) return 0;
			if(blockNum==1) return 3;
		}
		
		// real data
		switch((int)blockNum){
		case 0: return  0;
		case 1: return  10328;
		case 2: return  20373;
		case 3: return  30629;
		case 4: return  40645;
		case 5: return  50462;
		case 6: return  60841;
		case 7: return  70591;
		case 8: return  80118;
		case 9: return  90497;
		case 10: return 100501;
		case 11: return 110567;
		case 12: return 120945;
		case 13: return 130999;
		case 14: return 140574;
		case 15: return 150953;
		case 16: return 161332;
		case 17: return 171154;
		case 18: return 181514;
		case 19: return 191625;
		case 20: return 202004;
		case 21: return 212383;
		case 22: return 222762;
		case 23: return 232593;
		case 24: return 242878;
		case 25: return 252938;
		case 26: return 263149;
		case 27: return 273210;
		case 28: return 283473;
		case 29: return 293255;
		case 30: return 303043;
		case 31: return 313370;
		case 32: return 323522;
		case 33: return 333883;
		case 34: return 343663;
		case 35: return 353645;
		case 36: return 363929;
		case 37: return 374236;
		case 38: return 384554;
		case 39: return 394929;
		case 40: return 404712;
		case 41: return 414617;
		case 42: return 424747;
		case 43: return 434707;
		case 44: return 444489;
		case 45: return 454285;
		case 46: return 464398;
		case 47: return 474196;
		case 48: return 484050;
		case 49: return 493968;
		case 50: return 503752;
		case 51: return 514131;
		case 52: return 524510;
		case 53: return 534709;
		case 54: return 545088;
		case 55: return 555467;
		case 56: return 565846;
		case 57: return 576225;
		case 58: return 586604;
		case 59: return 596585;
		case 60: return 606367;
		case 61: return 616148;
		case 62: return 626448;
		case 63: return 636240;
		case 64: return 646022;
		case 65: return 655804;
		case 66: return 665666;
		case 67: return 675448;
		}
		return 685230;
		}
		
	/**
	 * @param blockNum
	 * @return the last page number assigned to blockNum
	 */
	public static long getBlockLastPage(long blockNum){
		// for testing with 6 nodes (2 blocks)
		if(debug) {
			if(blockNum == 0) return 2;
			else if(blockNum ==1) return 5;
		}
		
		// real data
		switch((int)blockNum){
		case 0: return 10327;
		case 1: return 20372;
		case 2: return 30628;
		case 3: return 40644;
		case 4: return 50461;
		case 5: return 60840;
		case 6: return 70590;
		case 7: return 80117;
		case 8: return 90496;
		case 9: return 100500;
		case 10: return 110566;
		case 11: return 120944;
		case 12: return 130998;
		case 13: return 140573;
		case 14: return 150952;
		case 15: return 161331;
		case 16: return 171153;
		case 17: return 181513;
		case 18: return 191624;
		case 19: return 202003;
		case 20: return 212382;
		case 21: return 222761;
		case 22: return 232592;
		case 23: return 242877;
		case 24: return 252937;
		case 25: return 263148;
		case 26: return 273209;
		case 27: return 283472;
		case 28: return 293254;
		case 29: return 303042;
		case 30: return 313369;
		case 31: return 323521;
		case 32: return 333882;
		case 33: return 343662;
		case 34: return 353644;
		case 35: return 363928;
		case 36: return 374235;
		case 37: return 384553;
		case 38: return 394928;
		case 39: return 404711;
		case 40: return 414616;
		case 41: return 424746;
		case 42: return 434706;
		case 43: return 444488;
		case 44: return 454284;
		case 45: return 464397;
		case 46: return 474195;
		case 47: return 484049;
		case 48: return 493967;
		case 49: return 503751;
		case 50: return 514130;
		case 51: return 524509;
		case 52: return 534708;
		case 53: return 545087;
		case 54: return 555466;
		case 55: return 565845;
		case 56: return 576224;
		case 57: return 586603;
		case 58: return 596584;
		case 59: return 606366;
		case 60: return 616147;
		case 61: return 626447;
		case 62: return 636239;
		case 63: return 646021;
		case 64: return 655803;
		case 65: return 665665;
		case 66: return 675447;
		case 67: return 685229;
		}
		return -1;
	}
	
	public @Override String toString(){
		return String.format("%d->%d (deg=%d)(PR=%f)", srcID, dstID, srcDeg, srcPR);
	}
}
